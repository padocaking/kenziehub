import styled from 'styled-components'

export const Container = styled.div`
background-color: #efefef;
color: #2a2a2a;
box-shadow: 0px 0px 10px 0px #cdcdcd;
padding: 15px;
margin-top: 15px;
width: 48%;
border-radius: 10px;
display: flex;
flex-direction: row;
align-items: center;
justify-content: space-between;

div {
    margin-left: 25px;
    display: flex;
    flex-direction: column;
    align-items: center;

    h2 {
    font-size: 22px;
    }

    h3 {
        font-size: 16px;
        font-weight: 400;
    }
}

.pencil {
    background-color: white;
    box-shadow: 0px 0px 2px gray;
}

.pencil img {
    filter: brightness(0);
}

picture {
    width: 30px;
    height: 30px;
    background-color: red;
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 5px;
    margin-top: 5px;
}

img {
    width: 20px;
    cursor: pointer;
    filter: invert(1);
}
`