import { Container } from './style'
import api from '../../services/api'
import { toast } from 'react-toastify'

function TechCard ({ title, status, id, loadAdds }) {
    const token = JSON.parse(localStorage.getItem("@KenzieHub:token"))

    const handleClick = (id) => {
        api.delete(`/users/techs/${id}`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        .then((_) => {
            toast.success("Tecnologia deletada com sucesso")
            loadAdds()
        })
        .catch((err) => console.log(err))
    }

    return (
        <Container>
            <div>
                <h2>
                    {title}
                </h2>
                <h3>
                    Status: {status}
                </h3>
            </div>
            <div>
                <picture className="pencil">
                    <img
                        src={"https://www.pikpng.com/pngl/b/77-776214_png-file-svg-pencil-icon-png-free-clipart.png"}
                        alt="Lixo"
                        onClick={() => toast.warn("Função ainda não existente")} />
                </picture>
                <picture>
                    <img
                        src={"https://simpleicon.com/wp-content/uploads/trash.png"}
                        alt="Lixo"
                        onClick={() => handleClick(id)} />
                </picture>
            </div>
            
            
        </Container>
    )
}

export default TechCard