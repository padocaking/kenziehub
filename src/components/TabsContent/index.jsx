import { AddContainer, FormContainer, Container, CardContainer, TecnologiasText } from './style'
import TechCard from '../TechCard'
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import api from '../../services/api';
import { toast } from 'react-toastify'

const useStyles = makeStyles((theme) => ({
    root: {
      '& > *': {
        margin: theme.spacing(1),
        width: '395px',
        height: '60px',
        marginBottom: '1rem',
      },

    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
      },
      selectEmpty: {
        marginTop: theme.spacing(2),
      },
}));

function TabsContent () {
    const classes = useStyles()

    const [titleInput, setTitleInput] = useState("")
    const [statusInput, setStatusInput] = useState("")
    const token = JSON.parse(localStorage.getItem("@KenzieHub:token"))

    const handleAdd = () => {

        api.post("/users/techs", {
            title: titleInput,
            status: statusInput,
        },
        {
            headers: {
                Authorization: `Bearer ${token}`
            },
        })
        .then(() => loadAdds())
        .catch((err) => toast.error("Tecnologia já registrada"))
    }

    const [tecnologias, setTecnologias] = useState([])
    const data = JSON.parse(localStorage.getItem("@KenzieHub:data"))

    const loadAdds = () => {
        api.get(`/users/${data.id}`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        .then((response) => setTecnologias(response.data.techs))
        .catch((err) => console.log(err))
    }

    useEffect(() => {
        loadAdds()
    }, [])

    return (
        <>
        <Container>
            <FormContainer>
            <form noValidate autoComplete="off">
            <div className={classes.root}>
                <TextField
                className="outlined-basic"
                label="Tecnologia"
                variant="outlined"
                value={titleInput}
                onChange={(e) => setTitleInput(e.target.value)}/>
            </div>
            <div className={classes.formControl}>
                <FormControl variant="outlined" value={statusInput}
                onChange={(e) => setStatusInput(e.target.value)}>
                    <InputLabel htmlFor="outlined-age-native-simple">Módulo</InputLabel>
                    <Select
                    native
                    label="Módulo">
                    <option aria-label="None" value="" />
                    <option value={"Iniciante"}>Iniciante</option>
                    <option value={"Intermediário"}>Intermediário</option>
                    <option value={"Avançado"}>Avançado</option>
                    </Select>
                </FormControl>
            </div>
            </form>
            </FormContainer>

            <AddContainer onClick={handleAdd}>
            <picture>
                <img
                src={"https://image.flaticon.com/icons/png/512/61/61183.png"}
                alt="Botão de adicionar"/>
            </picture>
            <h3>Adicionar tecnologia</h3>
            </AddContainer>
        </Container>

        <TecnologiasText>Tecnologias:</TecnologiasText>
        <CardContainer>
            {tecnologias.map((item) => (
                <TechCard key={item.index} title={item.title} status={item.status} id={item.id} loadAdds={loadAdds} />
            ))}
        </CardContainer>
        </>
    )
}

export default TabsContent