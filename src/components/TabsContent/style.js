import styled from 'styled-components'

export const Container = styled.div`
background-color: #f8f6ff;
padding: 10px;
border-radius: 10px;
box-shadow: 0px 0px 9px 0px #c8c8c8;
`

export const FormContainer = styled.div`
form {
    display: flex;
}
`

export const AddContainer = styled.div`
background-color: var(--lightblue);
color: white;
padding: 15px;
display: flex;
align-items: center;
border-radius: 8px;
cursor: pointer;

h3 {
    margin-left: 10px;
    font-size: 20px;
}

picture {
    width: 25px;
    height: 25px;

    img {
        width: 25px;
        filter: invert(1);
    }
}
`

export const TecnologiasText = styled.h2`
font-size: 32px;
text-align: center;
margin-top: 20px;
color: #494949;
`

export const CardContainer = styled.div`
display: flex;
flex-wrap: wrap;
justify-content: space-between;
margin-top: 5px;
`