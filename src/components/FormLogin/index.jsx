import { Container } from './style'
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import * as yup from 'yup';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import api from '../../services/api';
import { useHistory, Redirect } from 'react-router-dom'
import { toast } from 'react-toastify'

const useStyles = makeStyles((theme) => ({
    root: {
      '& > *': {
        margin: theme.spacing(1),
        width: '100%',
        height: '60px',
        marginBottom: '1rem',
      },

    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
      },
      selectEmpty: {
        marginTop: theme.spacing(2),
      },
}));

function FormRegister ({ authenticated, setAuthenticated }) {
    const history = useHistory()

    const classes = useStyles()

    const formSchema = yup.object().shape({
        email: yup
            .string()
            .required("E-mail obrigatório")
            .email("E-mail inválido"),
        password: yup
            .string()
            .required("Senha obrigatória")
            .min(6, "Deve conter pelo menos 6 caracteres"),
    })

    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm({
        resolver: yupResolver(formSchema)
    })

    const onSubmitFunction = (data) => {
        api.post("/sessions", data)
        .then((response) => {
            const { token } = response.data

            localStorage.setItem("@KenzieHub:token", JSON.stringify(token))
            localStorage.setItem("@KenzieHub:data", JSON.stringify(response.data.user))

            setAuthenticated(true)

            toast.success("Logado com sucesso!")

            return history.push("/perfil")
        })
        .catch((err) => toast.error("E-mail ou senha inválidos"))
    }

    if (authenticated) {
        return <Redirect to="/perfil"/>
    }

    return (
        <Container>
            <form
                onSubmit={handleSubmit(onSubmitFunction)}>
                <div className={classes.root}>
                    <TextField
                        className="outlined-basic"
                        label="E-mail"
                        variant="outlined"
                        {...register("email")}
                        error={!!errors.email}
                        helperText={errors.email?.message}/>
                </div>
                <div className={classes.root}>
                    <TextField
                        className="outlined-basic"
                        label="Senha" variant="outlined"
                        type="password"
                        {...register("password")}
                        error={!!errors.password}
                        helperText={errors.password?.message}/>
                </div>
                <button type="submit">Logar</button>
            </form>
        </Container>
    )
}

export default FormRegister