import styled from 'styled-components'

export const Container = styled.div`
margin: auto;
display: flex;
justify-content: space-between;
align-items: center;
width: 100%;
height: 75px;
background-color: var(--purple);
padding-left: 50px;
padding-right: 50px;
`

export const Nav = styled.div`
display: flex;

@media (max-width: 800px) {
    button {
        display: none;
    }
}

img {
    width: 55px;
    margin-right: 3rem;
}

button {
    color: white;
    background-color: #ffffff00;
    border: none;
    font-size: 1.5rem;
    margin-right: 3rem;
}
`

export const Form = styled.div`
display: flex;

@media (max-width: 800px) {
    button {
        display: none;
    }
}

button {
    text-transform: uppercase;
    border: none;
    font-size: 1.2rem;
    margin-right: 2rem;
    width: 7.5rem;
    height: 42px;
    border-radius: 8px;
}

.register {
    color: black;
    background-color: white;
}

.login {
    color: white;
    background-color: var(--lightblue);
}
`