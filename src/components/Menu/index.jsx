import { useHistory } from "react-router-dom"
import { Container, Nav, Form } from './style'
import kenzieLogo from '../../images/kenzie-logo.png'

function Menu () {
    const history = useHistory()

    const handleClick = (path) => history.push(path)

    return (
        <Container>
            <Nav>
                <img src={kenzieLogo} alt="Kenzie Logo"/>
                <button onClick={() => handleClick("/")}>Home</button>
                <button onClick={() => handleClick("/comunidade")}>Comunidade</button>
                <button onClick={() => handleClick("/perfil")}>Perfil</button>
            </Nav>
            <Form>
                <button onClick={() => handleClick("/register")} className="register">Register</button>
                <button onClick={() => handleClick("/login")} className="login">Login</button>
            </Form>
        </Container>
    )
}

export default Menu