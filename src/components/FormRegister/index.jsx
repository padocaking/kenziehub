import { Container } from './style'
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import * as yup from 'yup';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { useHistory, Redirect } from 'react-router-dom';
import api from '../../services/api'
import { toast } from 'react-toastify'

const useStyles = makeStyles((theme) => ({
    root: {
      '& > *': {
        margin: theme.spacing(1),
        width: '100%',
        height: '60px',
        marginBottom: '1rem',
      },

    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
      },
      selectEmpty: {
        marginTop: theme.spacing(2),
      },
}));

function FormRegister ({ authenticated }) {
    const history = useHistory()

    const classes = useStyles()

    const formSchema = yup.object().shape({
        name: yup
            .string()
            .required("Nome obrigatório"),
        email: yup
            .string()
            .required("E-mail obrigatório")
            .email("E-mail inválido"),
        contact: yup
            .string()
            .required("Contato obrigatório"),
        bio: yup
            .string()
            .required("Bio obrigatório"),
        course_module: yup
            .string()
            .required("Determinar módulo obrigatório"),
        password: yup
            .string()
            .required("Senha obrigatória")
            .min(6, "Deve conter pelo menos 6 caracteres"),
        passwordConfirm: yup
            .string()
            .required("Confimação obrigatória")
            .oneOf([yup.ref("password"), null], "As senhas devem ser iguais")

    })

    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm({
        resolver: yupResolver(formSchema)
    })

    const onSubmitFunction = ({ name, email, contact, bio, course_module, password }) => {
        const user = {
            name,
            email,
            contact,
            bio,
            course_module,
            password
        }
        api.post("/users", user)
        .then((_) => {
            toast.success("Sucesso ao criar a conta, faça seu Login")
            return history.push("/login")
        })
        .catch((_) => toast.error("Erro ao criar a conta, tente outro e-mail"))
    }

    if (authenticated) {
        return <Redirect to="/perfil"/>
    }

    return (
        <Container>
            <form
                onSubmit={handleSubmit(onSubmitFunction)}>
                <div className={classes.root}>
                    <TextField
                    className="outlined-basic"
                    label="Nome completo"
                    variant="outlined"
                    {...register("name")}
                    error={!!errors.name}
                    helperText={errors.name?.message}/>
                </div>
                <div className={classes.root}>
                    <TextField
                        className="outlined-basic"
                        label="E-mail"
                        variant="outlined"
                        {...register("email")}
                        error={!!errors.email}
                        helperText={errors.email?.message}/>
                </div>
                <div className={classes.root}>
                    <TextField
                        className="outlined-basic"
                        label="Contato"
                        variant="outlined"
                        {...register("contact")}
                        error={!!errors.contact}
                        helperText={errors.contact?.message}/>
                </div>
                <div className={classes.root}>
                    <TextField
                        className="outlined-basic"
                        label="Bio"
                        variant="outlined"
                        {...register("bio")}
                        error={!!errors.bio}
                        helperText={errors.bio?.message}/>
                </div>
                <div className={classes.root}>
                    <FormControl variant="outlined" className={classes.formControl}>
                        <InputLabel htmlFor="outlined-age-native-simple">Módulo</InputLabel>
                        <Select
                        native
                        label="Módulo"
                        {...register("course_module")}
                        error={!!errors.course_module}
                        helperText={errors.bio?.message}>
                        <option aria-label="None" value="" />
                        <option value={"Primeiro Módulo (Introdução ao Frontend)"}>Primeiro Módulo</option>
                        <option value={"Segundo Módulo (Frontend avançado)"}>Segundo Módulo</option>
                        <option value={"Terceiro Módulo (Introdução ao Backend)"}>Terceiro Módulo</option>
                        <option value={"Quarto Módulo (Backend avançado)"}>Quarto Módulo</option>
                        </Select>
                    </FormControl>
                </div>
                <div className={classes.root}>
                    <TextField
                        className="outlined-basic"
                        label="Senha" variant="outlined"
                        type="password"
                        {...register("password")}
                        error={!!errors.password}
                        helperText={errors.password?.message}/>
                </div>
                <div className={classes.root}>
                    <TextField
                        className="outlined-basic"
                        label="Confirmar senha" variant="outlined"
                        type="password"
                        {...register("passwordConfirm")}
                        error={!!errors.passwordConfirm}
                        helperText={errors.passwordConfirm?.message}/>
                </div>
                <button type="submit">Registrar</button>
            </form>
        </Container>
    )
}

export default FormRegister