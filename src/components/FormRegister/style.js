import styled from 'styled-components'

export const Container = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
width: 350px;
height: 710px;

button {
    margin-top: 1rem;
    text-transform: uppercase;
    border: none;
    font-size: 1.2rem;
    width: 23rem;
    height: 3.5rem;
    border-radius: 8px;
    margin-bottom: 1rem;
    background-color: #ffff;
    color: black;
    border: 2px solid black;
    }
`