import Menu from '../../components/Menu'
import { Container, RegisterDiv, BackgroundDiv } from './style'
import FormLogin from '../../components/FormLogin'
import Done from '../../images/done.svg'
import { Link } from 'react-router-dom'

function Login ({ authenticated, setAuthenticated }) {
    return (
        <>
        <Menu />
        <Container>
            <RegisterDiv>
                <h2>Logar-se</h2>
                <FormLogin
                    authenticated={authenticated}
                    setAuthenticated={setAuthenticated}/>
                <span>Não tem uma conta? <Link to="/register">Registre-se</Link></span>
            </RegisterDiv>
            <BackgroundDiv>
                <img src={Done} alt="BackgroundIMG"/>
            </BackgroundDiv>
        </Container>
        </>
    )
}

export default Login