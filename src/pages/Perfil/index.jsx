import { Redirect } from 'react-router-dom'
import { Container, InfoContainer, TechContainer, UserContainer } from './style'
import Menu from "../../components/Menu"
import PerfilTabs from '../../components/PerfilTabs'

function Perfil ({ authenticated }) {
    if (!authenticated) {
        return <Redirect to="/login" />
    }

    const userData = JSON.parse(localStorage.getItem("@KenzieHub:data"))

    console.log(userData)

    return (
        <>
        <Menu />
        <Container>
            <UserContainer>
                <InfoContainer>
                    <picture>
                        <img src={!!userData.avatar_url ? userData.avatar_url : "https://scott88lee.github.io/FMX/img/avatar.jpg"} alt="Usuário"/>
                    </picture>
                    <div>
                        <h2>{userData.name}</h2>
                        <h3>{userData.course_module}</h3>
                    </div>
                </InfoContainer>
                <TechContainer>
                    <PerfilTabs />
                </TechContainer>
            </UserContainer>
        </Container>
        </>
    )
}

export default Perfil