import styled from 'styled-components'

export const Container = styled.div`
background-image: var(--background);
min-height: calc(100vh - 75px);
padding-bottom: 50px;
padding-top: 15px;
display: flex;
justify-content: center;
`

export const UserContainer = styled.div`
width: 41rem;
background-image: var(--background);
box-shadow: 0px 0px 20px 0px #52525229;
padding: 1rem;
border-radius: 10px;
`

export const InfoContainer = styled.div`
display: flex;

picture {
width: 12rem;
height: 12rem;
border-radius: 6rem;

    img {
        width: 100%;
        border-radius: 6rem;
    }
}

div {
display: flex;
flex-direction: column;
justify-content: center;
margin-left: 20px;

    h2 {
        font-size: 3.5rem;
    }

    h3 {
        font-size: 1.25rem;
    }
}
`

export const TechContainer = styled.div`
`