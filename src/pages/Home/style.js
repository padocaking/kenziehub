import styled from 'styled-components'

export const Root = styled.div`
margin: auto;
`

export const Container = styled.div`
display: flex;
align-items: stretch;
justify-content: space-evenly;
height: calc(100vh - 75px);
//background-image: linear-gradient(to right top, #ffffff, #fafcff, #f0fbff, #e3faff, #d6faff);
background-image: var(--background);

@media (max-width: 800px) {
    img {
        display: none;
    }
}

img {
    width: 60%;
}

div {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    width: 50%;

    h3 {
        max-width: 30rem;
        text-align: center;
        font-size: 2rem;
        font-weight: 400;
        margin-bottom: 2rem;
    }

    button {
        text-transform: uppercase;
        border: none;
        font-size: 1.2rem;
        width: 20rem;
        height: 3.5rem;
        border-radius: 8px;
        margin-bottom: 1rem;
    }
    
    .getStarted {
        background-color: var(--lightblue);
        color: white;
    }

    .checkCommunity {
        background-color: white;
        border: 2px solid black;
    }

}
`