import Menu from '../../components/Menu'
import { Container, Root } from './style'
import Blogging from '../../images/blogging.svg'
import { useHistory } from 'react-router-dom'

function Home () {
    const history = useHistory()

    const handleClick = (path) => history.push(path)

    return (
        <Root>
            <Menu />
            <Container>
                <img src={Blogging} alt="Background Img"/>
                <div>
                    <h3>Começe já sua aventura no KenzieHub,
                        registre todas suas habilidades e trabalhos
                        enquanto acompanha a de seus colegas.
                    </h3>
                    <button onClick={() => handleClick("/register")} className="getStarted">COMEÇE JÁ!</button>
                    <button onClick={() => handleClick("/comunidade")} className="checkCommunity">CHECAR COMUNIDADE</button>
                </div>
            </Container>
        </Root>
    )
}

export default Home