import styled from 'styled-components'

export const Container = styled.div`
display: flex;
align-items: center;
justify-content: center;
height: calc(100vh - 75px);
background-image: var(--background);
`