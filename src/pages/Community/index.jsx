import Menu from '../../components/Menu'
import { Container } from './style'
import { Redirect } from 'react-router-dom'

function Community () {
    return (
        <>
        <Menu />
        <Container>
            <img src={"https://img.icons8.com/ios/452/under-construction.png"} alt="Em construção" />
        </Container>
        </>
    )
}

export default Community