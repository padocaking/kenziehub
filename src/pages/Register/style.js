import styled from 'styled-components'

export const Container = styled.div`
display: flex;
align-items: center;
justify-content: space-evenly;
width: 100%;
background-image: var(--background);
`

export const BackgroundDiv = styled.div`
img {
    width: 90%;
    margin-left: 1rem;
}
`

export const RegisterDiv = styled.div`
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
width: 30rem;
border-radius: 20px;
background-color: #ffffff;
box-shadow: 0px 0px 15px 0px #c6c6c6;
margin: 2rem;
margin-right: 3rem;

h2 {
    text-transform: uppercase;
    font-size: 2rem;
    margin: 1rem;
    margin-top: 2rem;
}

span {
    margin-top: -25px;
    padding-bottom: 25px;

    a {
        color: var(--lightblue);
    }
}
`