import Menu from '../../components/Menu'
import { Container, RegisterDiv, BackgroundDiv } from './style'
import FormRegister from '../../components/FormRegister'
import Form from '../../images/form.svg'
import { Link } from 'react-router-dom'

function Register () {
    return (
        <>
        <Menu />
        <Container>
            <BackgroundDiv>
                <img src={Form} alt="BackgroundIMG"/>
            </BackgroundDiv>
            <RegisterDiv>
                <h2>Registrar-se</h2>
                <FormRegister />
                <span>Já tem uma conta? Faça <Link to="/login">login</Link></span>
            </RegisterDiv>
        </Container>
        </>
    )
}

export default Register