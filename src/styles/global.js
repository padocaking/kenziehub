import { createGlobalStyle } from 'styled-components'

export default createGlobalStyle`
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    outline: 0;
}

:root {
    --purple: #4A0084;
    --lightblue: #50ABFF;
    --background: linear-gradient(to right top, #ffffff, #f9f8ff, #f5f0ff, #f1e9ff, #ede1ff);
}

body, input, button {
    font-family: 'Lato';
    font-size: 1rem;
}

button {
    cursor: pointer;
}

a {
    text-decoration: none;
}
`