import Home from '../pages/Home'
import Register from '../pages/Register'
import Login from '../pages/Login'
import Community from '../pages/Community'
import Perfil from '../pages/Perfil'
import { Switch, Route } from 'react-router-dom'
import { useState, useEffect } from 'react'

function Routes () {
    const [authenticated, setAuthenticated] = useState(false)

    useEffect(() => {
        const token = JSON.parse(localStorage.getItem("@KenzieHub:token"))

        if (token) {
            return setAuthenticated(true)
        }

    }, [authenticated])

    return (
        <Switch>
            <Route exact path="/">
                <Home authenticated={authenticated} />
            </Route>
            <Route path="/register">
                <Register authenticated={authenticated} />
            </Route>
            <Route path="/login">
                <Login
                    authenticated={authenticated}
                    setAuthenticated={setAuthenticated} />
            </Route>
            <Route path="/comunidade">
                <Community authenticated={authenticated} />
            </Route>
            <Route path="/perfil">
                <Perfil authenticated={authenticated} />
            </Route>
        </Switch>
    )
}

export default Routes